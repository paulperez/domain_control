<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- ENCABEZADO (START) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">

            <!-- TITULO DE LA PÁGINA -->
              <div class="col-sm-6">
                <div class="form-inline">
                  <h1 style="width: 50%;">Todos los Hosting</h1> 
                </div>
              </div>
            <!-- /.TITULO DE LA PÁGINA -->

            <!-- DIRECCIÓN DE LA PÁGINA -->
              <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Todos los hosting</li>
                </ol>
              </div>
            <!-- /.DIRECCIÓN DE LA PÁGINA -->

          </div>

          <?php if (isset($expired)) { ?>
          
            <!-- NOTIFICACIÓN DE DOMINIOS POR VENCER (START) -->
              <div class="col-md-12">
                <div class="card card-outline card-danger">
                  <div class="card-header">
                    <h3 class="card-title">Estos dominios venceran dentro de 15 días</h3>

                    <div class="card-tools">
                      <button type="button" class="btn btn-tool" data-card-widget="remove">
                        <i class="fas fa-times"></i>
                      </button>
                    </div>
                    <!-- /.card-tools -->
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    Todos los dominios próximos a vencer...
                  </div>
                  <!-- /.card-body -->
                </div>
                <!-- /.card -->
              </div>
            <!-- /NOTIFICACIÓN DE DOMINIOS POR VENCER (END) -->

          <?php } ?>

        </div>
      </div>
    <!-- /.ENCABEZADO (END) -->

    <!-- CONTENIDO PRINCIPAL (START) -->
      <section class="content">

        <!-- TABLA DE DOMINIOS -->
          <div class="row ">
            <div class="col-sm-12">
              <div class="form-group">
                <a href="<?= base_url('hosting/newhosting'); ?>" class="btn btn-dark btn-sm tooltip-2" style="background-color: #001f3f; margin-left: 18px">
                  Nuevo Hosting 
                </a> 
              </div>
            </div>
            <div class="col-sm-12">
              <div class="card">
                <div class="card-body table-responsive">
                  <!-- TABLE (START) -->
                    <table  id="hosting" class="table table-responsive-lg table-striped" cellspacing="0" width="100%">
                      <thead class="text-center">
                        <tr>
                          <th class="d-none align-middle">ID</th>
                          <th class="align-middle">Nombre</th>
                          <th class="align-middle d-none">Proveedor</th>
                          <th class="align-middle">Paquete</th>
                          <th class="align-middle">Servidor</th>
                          <th class="align-middle">Cliente</th>
                          <th class="align-middle">Dominio principal</th>
                          <th class="align-middle d-none">Fecha de creación</th>
                          <th class="align-middle">Expiracón</th>
                        </tr>
                      </thead>

                      <tbody class="text-truncate">
                        <?php foreach ($hosting_list as $key) { ?>
                          <tr style="cursor: pointer" data-toggle="modal" data-target="#view-domain<?= $key->id ?>">
                            <td class="align-middle d-none"><?= $key->id; ?></td>
                            <td class="align-middle"><?= $key->hosting_name; ?></td>
                            <td class="align-middle d-none"><?= $key->provider_name; ?></td>
                            <td class="align-middle"><?= $key->name_pack; ?></td>
                            <td class="align-middle"><?= $key->server_name; ?></td>
                            <td class="align-middle"><?= $key->customer_name; ?></td>
                            <td class="align-middle"><?= $key->domain_name; ?></td>
                            <td class="align-middle d-none"><?= $key->creation_date; ?></td>
                            <td class="align-middle"><?= $key->hosting_expiry; ?></td>
                          </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  <!-- /.TABLE (END) -->
                </div>
              </div>
            </div>
          </div>
        <!-- /.TABLA DE DOMINIOS (END) -->

      </section>
    <!-- /.CONTENIDO PRINCIPAL (END) -->

    <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
      </aside>
    <!-- /.control-sidebar -->
  </div>
<!-- /.Content Wrapper. Contains page content -->

<!-- MODALS -->
  <!-- view domain -->
    <?php setlocale(LC_TIME, "spanish");  foreach ($hosting_list as $key) { ?>
      <div class="modal fade" id="view-domain<?= $key->id ?>" tabindex="-1" role="dialog" aria-labelledby="view-domainLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel"><strong><?= strtoupper($key->hosting_name) ?></strong></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-4">
                  <!-- About Me Box -->
                  <div class="shadow card card-primary">
                    <div class="card-header bg-navy">
                      <h3 class="card-title">Acerca de</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                      <strong><i class="fas fa-truck"></i> Proveedor</strong>
                      <p class="text-muted">
                        <?= $key->provider_name ?>
                      </p>
                      <hr>
                      <strong><i class="fas fa-calendar-plus"></i> Fecha de creación</strong>
                      <p class="text-muted"><?= utf8_encode(strftime("%A %d de %B de %Y", strtotime($key->creation_date))); ?></p>
                      <hr>
                      <strong><i class="fas fa-calendar-minus"></i> Fecha de expiración</strong>
                      <p class="text-muted"><?= utf8_encode(strftime("%A %d de %B de %Y", strtotime($key->hosting_expiry))); ?></p>
                    </div>
                    <!-- /.card-body -->
                  </div>
                  <!-- /.card -->
                </div>
                <div class="col-8">
                  <table class="table">
                    <tr>
                      <td><strong>ID</strong></td>
                      <td><?= $key->id ?></td>
                    </tr>
                    <tr>
                      <td><strong>Dominio</strong></td>
                      <td><?= $key->hosting_name ?></td>
                    </tr>
                    <tr>
                      <td><strong>Paquete</strong></td>
                      <td><?= $key->name_pack ?></td>
                    </tr>
                    <tr>
                      <td><strong>Servidor</strong></td>
                      <td><?= $key->server_name ?></td>
                    </tr>
                    <tr>
                      <td><strong>Dominio</strong></td>
                      <td><?= $key->domain_name ?></td>
                    </tr>
                    <tr>
                      <td><strong>Cliente</strong></td>
                      <td><?= $key->customer_name ?></td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
              <a class="btn btn-dark bg-navy" href="<?= base_url('hosting/create/'.$key->id) ?>">Editar</a>
              <a class="btn btn-danger" href="<?= base_url('hosting/remove/'.$key->id) ?>">Eliminar</a>
            </div>
          </div>
        </div>
      </div>
    <?php } ?>
  <!-- /.view domain -->
<!-- /.MODALS -->