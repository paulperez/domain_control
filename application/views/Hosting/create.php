<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

  <!-- ENCABEZADO (START) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">

          <!-- TITULO DE LA PÁGINA -->
            <div class="col-sm-6">
              <div class="form-inline">
                <h1 style="width: 50%;">Crear Hosting</h1>
              </div>
            </div>
          <!-- /.TITULO DE LA PÁGINA -->

          <!-- DIRECCIÓN DE LA PÁGINA -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url() ?>">Inicio</a></li>
              <li class="breadcrumb-item"><a href="<?= base_url('hosting') ?>">Todos los Hosting</a></li>
              <li class="breadcrumb-item active">Nuevo Hosting</li>
              </ol>
            </div>
          <!-- /.DIRECCIÓN DE LA PÁGINA -->
        </div>

          <!-- NOTIFICACIÓN DE DOMINIOS POR VENCER (START) -->
            <?php $alerta = null; ?>
            <?php if (isset($alerta)) { ?>

              <div class="col-md-12">
                <div class="card card-outline card-danger">
                  <div class="card-header">
                    <h3 class="card-title">Estos dominios venceran dentro de 15 días</h3>

                    <div class="card-tools">
                      <button type="button" class="btn btn-tool" data-card-widget="remove">
                        <i class="fas fa-times"></i>
                      </button>
                    </div>
                    <!-- /.card-tools -->
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    Todos los dominios próximos a vencer...
                  </div>
                  <!-- /.card-body -->
                </div>
                <!-- /.card -->
              </div>

            <?php } ?>
          <!-- /NOTIFICACIÓN DE DOMINIOS POR VENCER (END) -->


      </div>
    </div>
  <!-- /.ENCABEZADO (END) -->


  <!-- CONTENIDO PRINCIPAL (START) -->
    <section class="content">

      <!-- TABLA DE DOMINIOS -->
        <div class="card">

          <div class="card-header">
            <h3 class="card-title">Hosting</h3>
          </div>

          <div class="card-body responsive">
          <?= isset($id) ? form_open('hosting/create/'.$id) : form_open('hosting/create') ;  ?>
          
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="exampleInputEmail1">Hosting</label>
                <input value="<?= $hosting_name_by_id ?>" name="hosting_name" type="text" class="form-control" placeholder="Nombre de Hosting">
              </div>
              <div class="form-group col-md-6">
                <label for="Nombre de Proveedor">Paquete</label>
                <select name="pack" class="form-control select-pack">
                <?php foreach ($paquetes as $key =>$p) { ?>
                  <option <?= $pack_by_id == $p->id ?  'selected' : '' ; ?> value="<?= $p->id ?>"><?= $p->name_pack ?> - <?= $p->storage ?> MB</option>
                <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="Nombre de Proveedor">Servidor</label>
                <select name="servers" class='select-pack form-control' style="">
                <?php foreach ($servidores as $key => $p) { ?>
                  <option <?= $servers_by_id == $p->id ?  'selected' : '' ; ?> value="<?= $p->id ?>"><?= $p->server_name ?></option>
                <?php } ?>
                </select>
              </div>
              <div class="form-group col-md-6">
                <label for="Nombre de Cliente">Cliente</label>
                <select name="customer" class='form-control select2'>
                  <?php foreach ($cliente as $key => $p) { ?>
                    <option <?= $customer_by_id == $p->id ?  'selected' : '' ; ?> value="<?= $p->id ?>"><?= $p->customer_name ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="exampleInputEmail1">Nombre de dominio</label>
                <input value="<?= $domain_name_by_id ?>" name="domain_name" type="text" class="form-control" placeholder="Nombre de Dominio">
              </div>
              <div class="form-group col-md-6">
                <label for="expiracion">Fecha de Expiración</label>
                <input value="<?= $expiration_date_by_id ?>" name="hosting_expiry" type="date" class="form-control datemask" placeholder="Fecha de Expiración">
              </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn bg-navy">Agregar</button>
                <a class="btn btn-secondary text-white">Cancelar</a>
            </div>
          <?= form_close() ?>
          </div>
          <!-- /.card-body -->
          <div class="card-footer">
          </div>
          <!-- /.card-footer-->
        </div>
      <!-- /.TABLA DE DOMINIOS (END) -->

    </section>
  <!-- /.CONTENIDO PRINCIPAL (END) -->


  <!-- MODALS -->
    <div class="modal fade" id="add_customer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
          <form class="" id="" method="post">
            <div class="form-group row mt-3">
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="fas fa-user-tie"></i></span>
                </div>
                <input type="text" id="customer_name" class="form-control" placeholder="Nombre del cliente" required>
              </div>
            </div>
            <div class="form-group row">
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                </div>
                <input type="email" id="customer_email" class="form-control" placeholder="Email" required>
              </div>
            </div>
            <div class="form-group row">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="fas fa-phone"></i></span>
                </div>
                <input type="text" id="customer_telephone" class="form-control" data-inputmask='"mask": "(999) 999-9999"' data-mask placeholde="Teléfono" required>
              </div>
            </div>
          </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" id="add" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </div>
    </div>
  <!-- /MODALS -->


  <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
  <!-- /.control-sidebar -->
</div>
<!-- /.Content Wrapper. Contains page content -->