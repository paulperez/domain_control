<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Domain Control</title>
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
    <!-- IonIcons -->
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <table class='table table-hover'>
                    <thead>
                        <tr>
                            <th colspan="7">
                                DOMINIOS
                            </th>
                            <th>
                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-default">
                                    Nuevo Dominio
                                </button>
                            </th>
                        </tr>
                        <tr>
                            <th>ID</th>
                            <TH>DOMINIO</TH>
                            <th>PROVEEDOR</th>
                            <th>TIPO</th>
                            <th>COSTO</th>
                            <th>PRECIO VENTA</th>
                            <th>EXPIRACIÓN</th>
                            <th>CLIENTE</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>001</td>
                            <td>paul.com</td>
                            <td>GoDaddy</td>
                            <td>.COM</td>
                            <td>$11.99</td>
                            <td>25</td>
                            <td>12/4/2020</td>
                            <td>Paul Perez</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="modal fade" id="modal-default">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Nuevo Dominio</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="card card-success">
                    <!-- form start -->
                    <form role="form">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Dominio</label>
                                <input type="text" class="form-control" id="nombrededominio" placeholder="Nombre de Dominio">
                            </div>
                            <div class="form-group">
                                <label for="Nombre de Proveedor">Proveedor</label>
                                <input type="text" class="form-control" id="nombrededominio" placeholder="Nombre del Proveedor">
                            </div>
                            <div class="form-group">
                                <label for="Nombre de Proveedor">Tipo</label>
                                <select name="" id="" class='form-control'>
                                    <option value="">.COM</option>
                                    <option value="">.NET</option>
                                    <option value="">.ORG</option>
                                    <option value="">.CO</option>
                                    <option value="">.COM.SV</option>
                                </select>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="Precio Costo">Precio Costo</label>
                                        <input type="number" class="form-control" id="preciocosto" placeholder="Precio Costo">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="Precio Venta">Precio Venta</label>
                                        <input type="number" class="form-control" id="precioventa" placeholder="Precio Venta">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="expiracion">Fecha de Expiración</label>
                                <input type="date" class="form-control" id="fechadeexpiracion" placeholder="Fecha de Expiración">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">File input</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="exampleInputFile">
                                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                <label class="form-check-label" for="exampleCheck1">Check me out</label>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </form>
                </div>

                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-success">Save changes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
      </div>
    </div>
    <script src="<?= base_url() ?>/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?= base_url() ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE -->
    <script src="<?= base_url() ?>/assets/js/adminlte.js"></script>

    <!-- OPTIONAL SCRIPTS -->
    <script src="<?= base_url() ?>/plugins/chart.js/Chart.min.js"></script>
    <script src="<?= base_url() ?>assets/js/demo.js"></script>
    <script src="<?= base_url() ?>assets/js/pages/dashboard3.js"></script>
</body>
</html>