  <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
        <div class="content-header">
          <div class="container-fluid">
            <div class="row mb-2">
              <div class="col-sm-6">
                <h1 class="m-0 text-dark">Inicio</h1>
              </div><!-- /.col -->
              <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item active">Inicio</li>
                </ol>
              </div><!-- /.col -->
            </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>
      <!-- /.content-header -->

      <!-- Main content -->
        <section class="content">
          <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
              <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-info elevation-3">
                  <div class="inner">
                    <h3><?= $allDomains ?></h3>

                    <p>Dominios</p>
                  </div>
                  <div class="icon">
                  <i class="fad fa-globe-americas"></i>
                  </div>
                  <a href="<?php base_url(""); ?>dominios" class="small-box-footer">Más Información <i class="fad fa-arrow-circle-right"></i></a>
                </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-success elevation-3">
                  <div class="inner">
                    <h3><?= $allHosting ?></h3>

                    <p>Hosting</p>
                  </div>
                  <div class="icon">
                    <i class="fad fa-hdd"></i>
                  </div>
                  <a href="<?= base_url('hosting/index') ?>" class="small-box-footer">Más Información <i class="fad fa-arrow-circle-right"></i></a>
                </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-warning elevation-3">
                  <div class="inner">
                    <h3>44</h3>

                    <p>Certificados</p>
                  </div>
                  <div class="icon">
                    <i class="fad fa-shield-alt"></i>
                  </div>
                  <a href="#" class="small-box-footer">Más Información <i class="fad fa-arrow-circle-right"></i></a>
                </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-danger elevation-3">
                  <div class="inner">
                    <h3>65</h3>

                    <p>Servidores</p>
                  </div>
                  <div class="icon">
                    <i class="fad fa-server"></i>
                  </div>
                  <a href="#" class="small-box-footer">Más Información <i class="fad fa-arrow-circle-right"></i></a>
                </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-purple elevation-3">
                  <div class="inner">
                    <h3>65</h3>

                    <p>Clientes</p>
                  </div>
                  <div class="icon">
                  <i class="fad fa-user-tie"></i>
                  </div>
                  <a href="#" class="small-box-footer">Más Información <i class="fad fa-arrow-circle-right"></i></a>
                </div>
              </div>
              <!-- ./col -->
            </div>
            <!-- /.row -->
            
          </div><!-- /.container-fluid -->
        </section>
      <!-- /.content -->
    </div>
  <!-- /.content-wrapper -->
  

  <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
  <!-- /.control-sidebar -->
