      </div>
    </div>

    <!-- PIE DE PÁGINA -->
      <footer class="main-footer fixed-bottom mt-5">
        <strong><a href="https://web-informatica.com/">Web Informática S. A. de C. V. &copy; <?= date('Y') ?></a></strong>
        All rights reserved.
        <div class="float-right d-none d-sm-inline-block">
          Powered By
          <b>Paúl Pérez</b>
        </div>
      </footer>
    <!-- PIE DE PÁGINA -->

    <!-- SCRIPTS -->
      <!-- jQuery -->
      <script src="<?= base_url(); ?>plugins/jquery/jquery.min.js"></script>
      <!-- jQuery UI 1.11.4 -->
      <script src="<?= base_url(); ?>plugins/jquery-ui/jquery-ui.min.js"></script>
      <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
      <script>
        $.widget.bridge('uibutton', $.ui.button)
      </script>
      <!-- Bootstrap 4 -->
      <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
      <!-- <script src="<?= base_url(); ?>plugins/fontawesome-pro/js/all.min.js"></script> -->
      <!-- DataTables -->
      <script src="<?= base_url(); ?>plugins/datatables/jquery.dataTables.js"></script>
      <script src="<?= base_url(); ?>plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
      <script src="<?= base_url(); ?>plugins/datatables-buttons/js/dataTables.buttons.js"></script>
      <script src="<?= base_url(); ?>plugins/jszip/jszip.js"></script>
      <script src="<?= base_url(); ?>plugins/pdfmake/pdfmake.js"></script>
      <script src="<?= base_url(); ?>plugins/pdfmake/vfs_fonts.js"></script>
      <script src="<?= base_url(); ?>plugins/datatables-buttons/js/buttons.html5.js"></script>
      <script src="<?= base_url(); ?>plugins/datatables-buttons/js/buttons.colVis.js"></script>
      <!-- ChartJS -->
      <script src="<?= base_url(); ?>plugins/chart.js/Chart.min.js"></script>
      <!-- JQVMap -->
      <script src="<?= base_url(); ?>plugins/jqvmap/jquery.vmap.min.js"></script>
      <script src="<?= base_url(); ?>plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
      <!-- jQuery Knob Chart -->
      <script src="<?= base_url(); ?>plugins/jquery-knob/jquery.knob.min.js"></script>
      <!-- daterangepicker -->
      <script src="<?= base_url(); ?>plugins/moment/moment.min.js"></script>
      <script src="<?= base_url(); ?>plugins/daterangepicker/daterangepicker.js"></script>
      <!-- Tempusdominus Bootstrap 4 -->
      <script src="<?= base_url(); ?>plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
      <!-- Summernote -->
      <script src="<?= base_url(); ?>plugins/summernote/summernote-bs4.min.js"></script>
      <!-- overlayScrollbars -->
      <script src="<?= base_url(); ?>plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
      <!-- Select2 -->
      <script src="<?= base_url(); ?>plugins/select2/js/select2.js"></script>
      <!-- bootstrap color picker -->
      <script src="<?= base_url(); ?>plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
      <!-- date-range-picker -->
      <script src="<?= base_url(); ?>plugins/daterangepicker/daterangepicker.js"></script>
      <!-- InputMask -->
      <script src="<?= base_url(); ?>plugins/moment/moment.min.js"></script>
      <script src="<?= base_url(); ?>plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
      <!-- SweetAlert2 -->
      <script src="<?= base_url(); ?>plugins/sweetalert2/sweetalert2.min.js"></script>
      <!-- Toastr -->
      <script src="<?= base_url(); ?>plugins/toastr/toastr.min.js"></script>
      <!-- AdminLTE App -->
      <script src="<?= base_url(); ?>assets/js/adminlte.js"></script>
      <!-- AdminLTE for demo purposes -->
      <script src="<?= base_url(); ?>assets/js/demo.js"></script>
      <script>
        $(document).ready(function () {
          var table = $('#dominios').DataTable(
          {
            responsive: true,
            dom: 
                "<'row'<'col-sm-6'B><'col-sm-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
            buttons:[
              {
                extend: 'excel',
                messageTop: 'Registro de dominios',
                text: '<text data-toggle="tooltip" title="Página actual" data-placement="top"><i class="fad fa-download"></i></text>',
                tittle: 'Exportar a Excel',
                className: 'btn btn-outline btn-sm btn-dark text-whit bg-navy',
                exportOptions: {modifier: {page: 'current'}},
                style: 'background-color: #001f3f'
              },{
                extend: 'excel',
                messageTop: 'Registro de todos dominios',
                text: '<text data-toggle="tooltip" title="Todos los registros" data-placement="top"><i class="fad fa-download"></i></text>',
                tittle: 'Exportar a Excel',
                className: 'btn btn-outline btn-sm btn-dark text-whit bg-navy',
                style: 'background-color: #001f3f'
              }
            ]
          }
        );
          
        $('#daterage').daterangepicker();
        });

        $(function() {
          //The passed argument has to be at least a empty object or a object with your desired options
          $("#body").overlayScrollbars(
            {
              overflow: 'none'
            }
          );
        });
      </script>

      <script>
      
          $(document).ready(function () {
            var table = $('#hosting').DataTable({
              responsive: true,
              dom: 
                  "<'row'<'col-sm-6'B><'col-sm-6'f>>" +
                  "<'row'<'col-sm-12'tr>>" +
                  "<'row'<'col-sm-4'i><'col-sm-4 text-center'l><'col-sm-4'p>>",
              buttons:[{
                  extend: 'excel',
                  messageTop: 'Registro de hosting',
                  text: '<text data-toggle="tooltip" title="Página actual" data-placement="top"><i class="fad fa-download"></i></text>',
                  titleAttr: 'Exportar a Excel',
                  className: 'btn btn-outline btn-sm btn-dark text-whit bg-navy',
                  exportOptions: {modifier: {page: 'current'}},
                  style: 'background-color: #001f3f'
                },{
                  extend: 'excel',
                  messageTop: 'Registro de todos los hosting',
                  text: '<text data-toggle="tooltip" title="Todos los registros" data-placement="top"><i class="fad fa-download"></i></text>',
                  titleAttr: 'Exportar a Excel',
                  className: 'btn btn-outline btn-sm btn-dark text-whit bg-navy',
                  style: 'background-color: #001f3f'
                }
              ]
            })
          })
      </script>
      <script src="https://cdn.jsdelivr.net/npm/clipboard@2.0.6/dist/clipboard.min.js"></script>
      <script>
        var clipboard = new ClipboardJS('.copyButton');

        Clipboard.on('success', function(e){
          alert("COPIADO CON ÉXITO");
        });

        Clipboard.on('error', function(e){
          //Algo no salio como debia
        });
      </script>
       
      <script>
        $(function () {
          //Initialize Select2 Elements
          $('.select2').select2({
            tags: true,
          })
          $('.select-pack').select2({
            tags: false,
          })


          //Datemask dd/mm/yyyy
          $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
          //Money Euro
          $('[data-mask]').inputmask()
        })
      </script>

      <script>
        $(document).on("click", "#add", function(e){
          e.preventDefault();

          var name = $("#name").val();
          var email = $("#email").val();

          if (name == "" || email == "") {
            alert("Valor requerido");
          }else{
            $.ajax({
              url: "<?= base_url('hosting/add_customer'); ?>",
              type: "post",
              dataType: "json",
              data: {
                name: customer_name,
                email: customer_email
              },
              success: function(data){
                console.log(data);
              }
            });
             
          }
        });
      </script>

      <Script>
        $('button.copyButton').click(function(){
          $(this).siblings('span.passwordCopy').select();      
          document.execCommand("copy");
        });

        $('#boton').onClick(function(){
          $('#').addClass('animate_class_name');
        });
          $('tooltip').tooltip()
      </Script>
      <script>
        $(function () {
          $('[data-toggle="tooltip"]').tooltip()
        })
      </script>
    <!-- /.SCRIPTS -->
  </body>
</body>
</html>