<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Sistema de Control</title>
  <link rel="stylesheet" href="<?= base_url('assets/css/style.css') ?>">
  <link rel="shortcut icon" href="https://web-informatica.com/wp-content/themes/webinformatica/img/favicon.png" type="image/x-icon">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url(); ?>plugins/fontawesome-pro/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="<?= base_url(); ?>plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?= base_url(); ?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="<?= base_url(); ?>plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?= base_url(); ?>plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?= base_url(); ?>plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="<?= base_url(); ?>plugins/summernote/summernote-bs4.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?= base_url(); ?>plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?= base_url(); ?>plugins/datatables-bs4/css/dataTables.bootstrap4.css">
  <link rel="stylesheet" href="<?= base_url(); ?>plugins/datatables-buttons/css/buttons.bootstrap4.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?= base_url(); ?>plugins/select2/css/select2.css">
  <link rel="stylesheet" href="<?= base_url(); ?>plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="<?= base_url(); ?>plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  <!-- Toastr -->
  <link rel="stylesheet" href="<?= base_url(); ?>plugins/toastr/toastr.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">

</head>
<body class="sidebar-mini layout-fixed text-sm pace-mac-osx-success accent-navy" style="font-family: 'Montserrat', sans-serif;">
<div class="">
  <!-- Navbar -->
    <nav class="main-header navbar navbar-expand fixed-top navbar-light border-bottom-0">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#"><i class="fal fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
          <a href="<?= base_url('') ?>" class="nav-link">Inicio</a>
        </li>
      </ul>

      <!-- SEARCH FORM -->
      <form class="form-inline ml-3">
        <div class="input-group input-group-sm">
          <input class="form-control form-control-navbar" type="search" placeholder="Buscar" aria-label="Buscar">
          <div class="input-group-append">
            <button class="btn btn-navbar" type="submit">
              <i class="fal fa-search"></i>
            </button>
          </div>
        </div>
      </form>

      <!-- Right navbar links -->
      <ul class="navbar-nav ml-auto">
        <!-- Messages Dropdown Menu -->
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="far fa-comments"></i>
            <span class="badge badge-danger navbar-badge">3</span>
          </a>

          <!-- MENSAJES -->
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <a href="#" class="dropdown-item">
              <!-- Message Start -->
              <div class="media">
                <img src="<?= base_url(); ?>assets/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
                <div class="media-body">
                  <h3 class="dropdown-item-title">
                    Brad Diesel
                    <span class="float-right text-sm text-danger"><i class="fal fa-star"></i></span>
                  </h3>
                  <p class="text-sm">Call me whenever you can...</p>
                  <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                </div>
              </div>
              <!-- Message End -->
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <!-- Message Start -->
              <div class="media">
                <img src="<?= base_url(); ?>assets/img/user8-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
                <div class="media-body">
                  <h3 class="dropdown-item-title">
                    John Pierce
                    <span class="float-right text-sm text-muted"><i class="fal fa-star"></i></span>
                  </h3>
                  <p class="text-sm">I got your message bro</p>
                  <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                </div>
              </div>
              <!-- Message End -->
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <!-- Message Start -->
              <div class="media">
                <img src="<?= base_url(); ?>assets/img/user3-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
                <div class="media-body">
                  <h3 class="dropdown-item-title">
                    Nora Silvester
                    <span class="float-right text-sm text-warning"><i class="fal fa-star"></i></span>
                  </h3>
                  <p class="text-sm">The subject goes here</p>
                  <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                </div>
              </div>
              <!-- Message End -->
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
          </div>
          <!-- MENSAJES (END) -->

        </li>
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="far fa-bell"></i>
            <span class="badge badge-warning navbar-badge">15</span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <span class="dropdown-item dropdown-header">15 Notifications</span>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fal fa-envelope mr-2"></i> 4 new messages
              <span class="float-right text-muted text-sm">3 mins</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fal fa-users mr-2"></i> 8 friend requests
              <span class="float-right text-muted text-sm">12 hours</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fal fa-file mr-2"></i> 3 new reports
              <span class="float-right text-muted text-sm">2 days</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
          </div>
        </li>
        <!-- Notifications Dropdown Menu (END) -->
        <li class="nav-item"> 
          <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#">
            <i class="fal fa-th-large"></i>
          </a>
        </li>
      </ul>
    </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
    <aside class="main-sidebar elevation-4 sidebar-light-navy">
      <!-- Brand Logo -->
      <a href="<?= base_url('') ?>" class="brand-link navbar-white">
        <img src="<?= base_url(); ?>assets/img/AdminLTELogo.svg" alt="AdminLTE Logo" class="brand-image"
            style="opacity: .8;filter: drop-shadow(5px 5px 5px #222);height: 17px!important;margin-bottom: -.25rem;margin-left: 12px!important;margin-top: 3px!important;">
        <span class="brand-text font-weight-light">DHM</span>
      </a>

      <!-- Sidebar -->
      <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
          <div class="image">
            <img src="<?= base_url(); ?>assets/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
          </div>
          <div class="info">
            <a href="#" class="d-block">Nombre de Usuario</a>
          </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
                with font-awesome or any other icon font library -->

            <!-- Dominios -->
              <li class="nav-item has-treeview <?= isset($domain_menu) ? 'menu-open' : '' ; ?><?= isset($add_domain) ? 'menu-open' : '' ; ?>">
                <a href="#" class="nav-link  <?= isset($domain_menu)  ? 'active' : '' ; ?><?= isset($add_domain) ? 'active' : '' ; ?>">
                  <i class="nav-icon fal fa-globe"></i>
                  <p>
                    Dominios
                    <i class="right fal fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="<?= base_url(); ?>dominios" class="nav-link <?= isset($domain_menu)  ? 'active' : '' ; ?>">
                      <i class="fal fa-circle nav-icon"></i>
                      <p>Todos los dominios</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="<?= base_url('dominios/newdomain'); ?>" class="nav-link <?= isset($add_domain)  ? 'active' : '' ; ?>">
                      <i class="fal fa-circle nav-icon"></i>
                      <p>Nuevo dominios</p>
                    </a>
                  </li>
                </ul>
              </li>
            <!-- /Dominios -->

            <!-- Hosting -->
              <li class="nav-item has-treeview <?= isset($hosting_menu) ? 'menu-open' : '' ; ?><?= isset($add_hosting) ? 'menu-open' : '' ; ?>">
                <a href="#" class="nav-link  <?= isset($hosting_menu)  ? 'active' : '' ; ?><?= isset($add_hosting) ? 'active' : '' ; ?>">
                  <i class="nav-icon far fa-hdd"></i>
                  <p>
                    Hosting
                    <i class="right fal fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="<?= base_url('hosting/index') ?>" class="nav-link <?= isset($hosting_menu)  ? 'active' : '' ; ?>">
                      <i class="fal fa-circle nav-icon"></i>
                      <p>Todos los Hosting</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="<?= base_url('hosting/newhosting') ?>" class="nav-link <?= isset($add_hosting)  ? 'active' : '' ; ?>">
                      <i class="fal fa-circle nav-icon"></i>
                      <p>Agregar Hosting</p>
                    </a>
                  </li>
                </ul>
              </li>
            <!-- /Hosting -->

            <!-- Certificados -->
              <li class="nav-item has-treeview">
                <a href="#" class="nav-link <?= $this->uri->segment(1) == 'certificados' ? 'active' : '' ?>">
                  <i class="nav-icon fal fa-shield-alt"></i>
                  <p>
                    Certificados
                    <i class="right fal fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="./index.html" class="nav-link">
                      <i class="fal fa-circle nav-icon"></i>
                      <p>Todos los dominios</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="./index2.html" class="nav-link">
                      <i class="fal fa-circle nav-icon"></i>
                      <p>Agregar dominios</p>
                    </a>
                  </li>
                </ul>
              </li>
            <!-- /Certificados -->
            
            <!-- Servidores -->
              <li class="nav-item has-treeview">
                <a href="#" class="nav-link <?= $this->uri->segment(1) == 'servidores' ? 'active' : '' ?>">
                  <i class="nav-icon fal fa-server"></i>
                  <p>
                    Servidores
                    <i class="right fal fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="./index.html" class="nav-link">
                      <i class="fal fa-circle nav-icon"></i>
                      <p>Todos los dominios</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="./index2.html" class="nav-link">
                      <i class="fal fa-circle nav-icon"></i>
                      <p>Agregar dominios</p>
                    </a>
                  </li>
                </ul>
              </li>
            <!-- /Servidores -->

            <!-- Proveedores -->
              <li class="nav-item has-treeview">
                <a href="#" class="nav-link <?= $this->uri->segment(1) == 'servidores' ? 'active' : '' ?>">
                  <i class="nav-icon fal fa-truck"></i>
                  <p>
                    Proveedores
                    <i class="right fal fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="./index.html" class="nav-link">
                      <i class="fal fa-circle nav-icon"></i>
                      <p>Todos los proveedores</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="./index2.html" class="nav-link">
                      <i class="fal fa-circle nav-icon"></i>
                      <p>Agregar proveedor</p>
                    </a>
                  </li>
                </ul>
              </li>
            <!-- /Proveedores -->

            <!-- Cuentas -->
              <li class="nav-item has-treeview">
                <a href="#" class="nav-link <?= $this->uri->segment(1) == 'servidores' ? 'active' : '' ?>">
                  <i class="nav-icon fal fa-key"></i>
                  <p>
                    Cuentas
                    <i class="right fal fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="./index.html" class="nav-link">
                      <i class="fal fa-circle nav-icon"></i>
                      <p>Todos las cuentas</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="./index2.html" class="nav-link">
                      <i class="fal fa-circle nav-icon"></i>
                      <p>Agregar cuenta</p>
                    </a>
                  </li>
                </ul>
              </li>
            <!-- /Cuentas -->

            <!-- Clientes -->
              <li class="nav-item has-treeview">
                <a href="#" class="nav-link <?= $this->uri->segment(1) == 'servidores' ? 'active' : '' ?>">
                  <i class="nav-icon fal fa-user-tie"></i>
                  <p>
                    Clientes
                    <i class="right fal fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="./index.html" class="nav-link">
                      <i class="fal fa-circle nav-icon"></i>
                      <p>Todos las clientes</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="./index2.html" class="nav-link">
                      <i class="fal fa-circle nav-icon"></i>
                      <p>Agregar clientes</p>
                    </a>
                  </li>
                </ul>
              </li>
            <!-- /Clientes -->
            <li class="nav-header">OTROS</li>
            <li class="nav-item  <?= $this->uri->segment(1) == 'calendario' ? 'active' : '' ?>">
              <a href="pages/calendar.html" class="nav-link">
                <i class="nav-icon far fa-calendar-alt"></i>
                <p>
                  Calendar
                  <span class="badge badge-info right">2</span>
                </p>
              </a>
            </li>
          </ul>
        </nav>
        <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
    </aside>
  <!-- /.Main Sidebar Container -->

  <div class="mt-5 mb-5 body">