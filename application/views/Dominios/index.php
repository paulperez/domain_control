<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 550px!important">
    <!-- ENCABEZADO (START) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <!-- TITULO DE LA PÁGINA -->
              <div class="col-sm-6">
                <div class="form-inline">
                  <h1 style="width: 50%;">Todos los Dominios</h1> 
                </div>
              </div>
            <!-- /.TITULO DE LA PÁGINA -->

            <!-- DIRECCIÓN DE LA PÁGINA -->
              <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                <li class="breadcrumb-item active">Todos los dominios</li>
                </ol>
              </div>
            <!-- /.DIRECCIÓN DE LA PÁGINA -->
          </div>

          <?php if (isset($expired)) { ?>
          
            <!-- NOTIFICACIÓN DE DOMINIOS POR VENCER (START) -->
              <div class="col-md-12">
                <div class="card card-outline card-danger">
                  <div class="card-header">
                    <h3 class="card-title">Estos dominios venceran dentro de 15 días</h3>

                    <div class="card-tools">
                      <button type="button" class="btn btn-tool" data-card-widget="remove">
                        <i class="fas fa-times"></i>
                      </button>
                    </div>
                    <!-- /.card-tools -->
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    Todos los dominios próximos a vencer...
                  </div>
                  <!-- /.card-body -->
                </div>
                <!-- /.card -->
              </div>
            <!-- /NOTIFICACIÓN DE DOMINIOS POR VENCER (END) -->

          <?php } ?>

        </div>
      </div>
    <!-- /.ENCABEZADO (END) -->

    <!-- CONTENIDO PRINCIPAL (START) -->
      <section class="content">

        <!-- TABLA DE DOMINIOS -->
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <a href="<?= base_url('dominios/newdomain'); ?>" class="btn btn-dark btn-sm" style="background-color: #001f3f; margin-left: 18px">
                  Nuevo Dominio 
                </a> 
              </div>
            </div>
            <div class="col-sm-12">
              <div class="card">
                <div class="card-body table-responsive">
                  <!-- TABLE (START) -->
                    <table  id="dominios" class="table table-responsive-lg table-hover align-middle" cellspacing="0" width="100%">
                      <thead class="text-center">
                        <tr>
                          <th class="align-middle d-none ocultar">ID</th>
                          <th class="align-middle">Dominios</th>
                          <th class="align-middle">Proveedor</th>
                          <th class="align-middle">Costo</th>
                          <th class="align-middle">Venta</th>
                          <th class="align-middle d-none ocultar">Creación</th>
                          <th class="align-middle">Expiracón</th>
                          <th class="align-middle">Cliente</th>
                          <th class="align-middle d-none ocultar">Cuenta</th>
                        </tr>
                      </thead>

                      <tbody class="text-truncate" style="cursor:context-menu">
                        <?php foreach ($dominios as $key) { ?>
                          <tr data-toggle="modal" data-target="#view-domain<?= $key['id'] ?>">
                            <td class="d-none align-middle"><?= $key['id']; ?></td>
                            <td class="align-middle"><?= $key['domain_name']; ?></td>
                            <td class="align-middle"><?= $key['provider_name']; ?></td>
                            <td class="align-middle">$<?= $key['cost']; ?></td>
                            <td class="align-middle">$<?= $key['sale']; ?></td>
                            <td class="d-none align-middle"><?= utf8_encode(strftime("%d %b %Y", strtotime($key['creation_date']))); ?></td>
                            <td class="align-middle"><?= utf8_encode(strftime("%d %b %Y", strtotime($key['expiration_date']))); ?></td>
                            <td class="align-middle"><?= $key['customer_name']; ?></td>
                            <td class="d-none align-middle"><?= $key['account']; ?></td>
                          </tr>
                        <?php } ?>
                      </tbody>

                    </table>
                  <!-- /.TABLE (END) -->
                </div>
              </div>
            </div>
          </div>
        <!-- /.TABLA DE DOMINIOS (END) -->

      </section>
    <!-- /.CONTENIDO PRINCIPAL (END) -->

    <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
      </aside>
    <!-- /.control-sidebar -->
  </div>
<!-- /.Content Wrapper. Contains page content -->

<!-- MODALS -->
  <!-- view domain -->
    <?php setlocale(LC_TIME, "spanish");  foreach ($dominios as $key) { ?>
      <div class="modal fade" id="view-domain<?= $key['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="view-domainLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel"><strong><?= strtoupper($key['domain_name']) ?></strong></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-4">
                  <!-- About Me Box -->
                  <div class="shadow card card-primary">
                    <div class="card-header bg-navy">
                      <h3 class="card-title ">Acerca de <?= $key['domain_name'] ?></h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                      <strong><i class="fas fa-truck"></i> Proveedor</strong>

                      <p class="text-muted">
                        <?= $key['provider_name'] ?>
                      </p>

                      <hr>

                      <strong><i class="fas fa-calendar-minus"></i> Fecha de creación</strong>

                      <p class="text-muted"><?= utf8_encode(strftime("%A %d de %B de %Y", strtotime($key['creation_date']))); ?></p>

                      <hr>

                      <strong><i class="fas fa-calendar-minus"></i> Fecha de expiración</strong>

                      <p class="text-muted"><?= utf8_encode(strftime("%A %d de %B de %Y", strtotime($key['expiration_date']))); ?></p>
                    
                    </div>
                    <!-- /.card-body -->
                  </div>
                  <!-- /.card -->
                </div>
                <div class="col-8">
                  <table class="table">
                    <tr>
                      <td><strong>ID</strong></td>
                      <td><?= $key['id'] ?></td>
                    </tr>
                    <tr>
                      <td><strong>Dominio</strong></td>
                      <td><?= $key['domain_name'] ?></td>
                    </tr>
                    <tr>
                      <td><strong>Costo</strong></td>
                      <td>$<?= $key['cost'] ?></td>
                    </tr>
                    <tr>
                      <td><strong>Precio Venta</strong></td>
                      <td>$<?= $key['sale'] ?></td>
                    </tr>
                    </tr>
                      <td><strong>Cliente</strong></td>
                      <td><?= $key['customer_name'] ?></td>
                    </tr>
                    </tr>
                      <td><strong>Cuenta</strong></td>
                      <td><?= $key['account'] ?></td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
              <a href="<?= base_url('dominios/create/'.$key['id']) ?>" type="button" class="btn btn-dark bg-navy">Editar</a>
              <a href="<?= base_url('dominios/remove/'.$key['id']) ?>" type="button" class="btn btn-danger">Eliminar</a>
            </div>
          </div>
        </div>
      </div>
    <?php } ?>
  <!-- /.view domain -->
<!-- /.MODALS -->