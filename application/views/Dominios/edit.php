
    <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">

        <!-- ENCABEZADO (START) -->
          <div class="content-header">
            <div class="container-fluid">
              <div class="row mb-2">

                <!-- TITULO DE LA PÁGINA -->
                  <div class="col-sm-6">
                    <div class="form-inline">
                      <h1 style="width: 50%;">Editar Dominio</h1>
                    </div>
                  </div>
                <!-- /.TITULO DE LA PÁGINA -->

                <!-- DIRECCIÓN DE LA PÁGINA -->
                  <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?= base_url() ?>">Inicio</a></li>
                    <li class="breadcrumb-item"><a href="<?= base_url('dominios') ?>">Todos los Dominios</a></li>
                    <li class="breadcrumb-item active">Editar Dominio</li>
                    </ol>
                  </div>
                <!-- /.DIRECCIÓN DE LA PÁGINA -->
              </div>

                <!-- NOTIFICACIÓN DE DOMINIOS POR VENCER (START) -->
                  <?php $alerta = null; ?>
                  <?php if (isset($alerta)) { ?>

                    <div class="col-md-12">
                      <div class="card card-outline card-danger">
                        <div class="card-header">
                          <h3 class="card-title">Estos dominios venceran dentro de 15 días</h3>

                          <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="remove">
                              <i class="fas fa-times"></i>
                            </button>
                          </div>
                          <!-- /.card-tools -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                          Todos los dominios próximos a vencer...
                        </div>
                        <!-- /.card-body -->
                      </div>
                      <!-- /.card -->
                    </div>

                  <?php } ?>
                <!-- /NOTIFICACIÓN DE DOMINIOS POR VENCER (END) -->


            </div>
          </div>
        <!-- /.ENCABEZADO (END) -->

        <!-- CONTENIDO PRINCIPAL (START) -->
          <section class="content">

            <!-- FORMULARIO DE DOMINIOS -->
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Dominios</h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                      <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="maximize" data-toggle="tooltip" title="Maximize">
                      <i class="fas fa-expand"></i></button>
                  </div>
                </div>
                <div class="card-body responsive">
                <?= form_open('dominios/edit'.$edit['id']) ?>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Dominio</label>
                    <input name="domain_name" type="text" class="form-control" placeholder="Nombre de Dominio">
                  </div>
                  <div class="row">
                    <div class="col-6 form-group">
                      <label for="Nombre de Proveedor">Proveedor</label>
                      <select name="id_provider" class="form-control select2">
                      <?php foreach ($providers as $key) { ?>
                        <option value="<?= $key['id'] ?>"><?= $key['provider_name'] ?></option>
                      <?php } ?>
                      </select>
                    </div>
                    <div class="col-6 form-group">
                      <label for="Nombre de Proveedor">Tipo</label>
                      <select name="type"  class='form-control select2'>
                      <?php foreach ($type as $key) { ?>
                        <option value="<?= $key['id'] ?>"><?= $key['type'] ?></option>
                      <?php } ?>
                      </select>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-6">
                      <div class="form-group">
                        <label for="Precio Costo">Precio Costo</label>
                        <input name="cost" type="number" step="any" class="form-control" placeholder="Precio Costo">
                      </div>
                    </div>
                    <div class="col-6">
                      <div class="form-group">
                        <label for="Precio Venta">Precio Venta</label>
                        <input name="sale" type="number" step="any" class="form-control" placeholder="Precio Venta">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-6">
                      <label for="creación">Fecha de Creación</label>
                      <input name="creation_date" type="date" class="form-control datemask" placeholder="Fecha de Creación" data-inputmask-alias="datetime" data-inputmask-inputformat="mm/dd/yyyy" data-mask>
                    </div>
                    <div class="form-group col-6">
                      <label for="expiracion">Fecha de Expiración</label>
                      <input name="expiration_date" type="date" class="form-control datemask" placeholder="Fecha de Expiración">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="Nombre de Cliente">Cliente</label>
                    <select name="id_customer" class="form-control select2">
                    <?php foreach ($customer as $key) { ?>
                      <option value="<?= $key['id'] ?>"><?= $key['customer_name'] ?></option>
                    <?php } ?>
                    </select>
                  </div>
                  <div class="form-group">
                      <button type="submit" class="btn btn-success">Agregar</button>
                      <a class="btn btn-danger text-white">Cancelar</a>
                  </div>
                <?= form_close() ?>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                </div>
                <!-- /.card-footer-->
              </div>
            <!-- /.TABLA DE DOMINIOS (END) -->

          </section>
        <!-- /.CONTENIDO PRINCIPAL (END) -->

        <!-- Control Sidebar -->
          <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
          </aside>
        <!-- /.control-sidebar -->
      </div>
    <!-- /.Content Wrapper. Contains page content -->
   