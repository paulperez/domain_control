<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
            <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">

                    <!-- TITULO DE LA PÁGINA -->
                        <div class="col-sm-6">
                            <div class="form-inline">
                            <h1 style="width: 50%;">Nuevo Dominio</h1>
                            </div>
                        </div>
                    <!-- /.TITULO DE LA PÁGINA -->

                    <!-- DIRECCIÓN DE LA PÁGINA -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="<?= base_url() ?>">Inicio</a></li>
                            <li class="breadcrumb-item"><a href="<?= base_url('dominios') ?>">Todos los Dominios</a></li>
                            <li class="breadcrumb-item active">Nuevo Dominio</li>
                            </ol>
                        </div>
                    <!-- /.DIRECCIÓN DE LA PÁGINA -->

                </div>
            </div><!-- /.container-fluid -->
            </section>
        <!-- /.Content Header (Page header) -->

        <!-- Main content -->
            <section class="content">

            <!-- Default box -->
            <div class="card">
                <div class="card-header">
                <h3 class="card-title" style="margin-top: 6px; margin-bottom: 3px;">Información previa</h3>
                
                    <a href="<?= base_url('dominios/create'); ?>" class="btn btn-dark btn-sm" style="background-color: #001f3f; margin-left: 18px">
                        Agregar <i class="fas fa-plus-circle"></i> 
                    </a> 
                </div>

                <!-- card-body -->
                <div class="card-body">
                Antes de añadir un nuevo dominio debe verificar la existencia de la siguiente información:<br><br>

                    <!-- card-box -->
                        <div class="row">
                            <div style="cursor: pointer;" class="col-4" data-toggle="modal" data-target="#provider">
                                <div class="info-box">
                                <span class="info-box-icon bg-info"><i class="fas fa-truck"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Proveedor</span>
                                    <span class="info-box-number">10</span>
                                </div>
                                <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                            <div style="cursor: pointer;" class="col-4" data-toggle="modal" data-target="#customer">
                                <div class="info-box">
                                    <span class="info-box-icon bg-warning"><i class="fas fa-user-tie"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Cliente</span>
                                        <span class="info-box-number">50</span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                            <div style="cursor: pointer;" class="col-4" data-toggle="modal" data-target="#accounts">
                                <div class="info-box">
                                <span class="info-box-icon bg-danger"><i class="fas fa-key"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Cuentas</span>
                                    <span class="info-box-number">93,139</span>
                                </div>
                                <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                            </div>
                        </div>
                    <!-- /.card-box -->

                <!-- /.card-body -->
                
                <!-- card-footer-->
                <div class="card-footer">
                Footer
                </div>
                <!-- /.card-footer-->
            </div>
            <!-- /.card -->

            </section>
        <!-- /.Main content -->
    </div>
<!-- /.content-wrapper -->

<!-- MODALS -->

    <!-- Modal Porveedor -->
        <div class="modal fade" id="provider" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Proveedores</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-4" style="height: 25rem;max-height: 25rem;">
                                <div class="overflow-auto os-viewport os-viewport-native-scrollbars-invisible">
                                    <div class="list-group" id="list-tab" role="tablist">
                                    <?php foreach($providers as $key) { ?>    
                                        <div 
                                            class="text-truncate list-group-item list-group-item-action 
                                                <?= $first_provider->id == $key->id ? 'active' : '' ?>"
                                            id="list-<?= $key->id ?>-list" 
                                            data-toggle="list" 
                                            href="#list-<?= $key->id ?>" 
                                            role="tab" 
                                            aria-controls="<?= $key->id ?>">
                                                <?= $key->provider_name; ?>
                                        </div>
                                    <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-8">
                                <div class="tab-content" id="nav-tabContent">
                                    <?php $copy=1; ?>
                                    <?php foreach($providers as $key) { $copy++;?>    
                                        <div class="tab-pane fade <?= $first_provider->id == $key->id ? 'show active' : '' ?>" id="list-<?= $key->id ?>" role="tabpanel" aria-labelledby="list-<?= $key->id ?>-list">
                                            <h3 class="text-truncate"><?= $key->provider_name ?></h3>
                                            <div style="cursor:pointer" class="form-inline">
                                                <i class="fas fa-envelope-open-text mr-2"></i>
                                                <p style="user-select: all!important;" id="text-copy-proveedor-1-<?= $copy ?>"><?= $key->support_email ?></p>
                                                <button data-clipboard-target="#text-copy-proveedor-1-<?= $copy ?>" class="copyButton badge badge-secondary mb-3 ml-2"><i class="far fa-clipboard"></i> Copy</button> 
                                            </div>
                                            <div style="cursor:pointer" class="form-inline">
                                                <i class="fas fa-phone-alt mr-2"></i>
                                                <p style="user-select: all!important;" id="text-copy-proveedor-2-<?= $copy ?>"><?= $key->support_telephone ?></p>
                                                <button data-clipboard-target="#text-copy-proveedor-2-<?= $copy ?>"  class="copyButton badge badge-secondary mb-3 ml-2"><i class="far fa-clipboard"></i> Copy</button> 
                                            </div>

                                            <!-- Small Box (Stat card) -->
                                            <h5 class="mb-2 mt-4">Productos</h5>
                                            <div class="row">
                                                <!-- Conteo de dominios -->
                                                <div class="col-sm-6">
                                                    <a href="<?= base_url('dominios') ?>">
                                                        <div class="info-box bg-info">
                                                            <span class="info-box-icon"><i class="fas fa-globe"></i></span>

                                                            <div class="info-box-content">
                                                                <span class="info-box-text">Dominios</span>
                                                                <span class="info-box-number"><?= $key->domains_count ?></span>
                                                            </div>
                                                        <!-- /.info-box-content -->
                                                        </div>
                                                        <!-- /.info-box -->
                                                    </a>
                                                </div>
                                                <!-- Conteo de Hosting -->
                                                <div class="col-sm-6">
                                                    <a href="<?= base_url('hosting') ?>">
                                                        <div class="info-box bg-success">
                                                            <span class="info-box-icon"><i class="far fa-hdd"></i></span>

                                                            <div class="info-box-content">
                                                                <span class="info-box-text">Hosting</span>
                                                                <span class="info-box-number"><?= $key->hosting_count ?></span>
                                                            </div>
                                                            <!-- /.info-box-content -->
                                                        </div>
                                                        <!-- /.info-box -->
                                                    </a>
                                                </div>
                                                <!-- Conteo de Certificados SSL -->
                                                <div class="col-sm-6">
                                                    <div class="info-box bg-warning">
                                                    <span class="info-box-icon"><i class="fas fa-shield-alt"></i></span>

                                                    <div class="info-box-content">
                                                        <span class="info-box-text">Certificados SSL</span>
                                                        <span class="info-box-number">41,410</span>
                                                    </div>
                                                    <!-- /.info-box-content -->
                                                    </div>
                                                    <!-- /.info-box -->
                                                </div>
                                                <!-- Conteo de Servidores -->
                                                <div class="col-sm-6">
                                                    <div class="info-box bg-danger">
                                                    <span class="info-box-icon"><i class="fas fa-server"></i></span>

                                                    <div class="info-box-content">
                                                        <span class="info-box-text">Servidores</span>
                                                        <span class="info-box-number">41,410</span>
                                                    </div>
                                                    <!-- /.info-box-content -->
                                                    </div>
                                                    <!-- /.info-box -->
                                                </div>
                                            </div>
                                            <!-- /.row -->    
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Regresar <i class="fas fa-arrow-left"></i></button>
                        <button type="button" style="border-color: #001f3f;" class="bg-navy btn btn-primary" data-toggle="modal" data-target="#newProvider">
                            Nuevo proveedor
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Nuevos Proveedor -->
            <div class="modal fade" id="newProvider" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Nuevo Proveedor</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="<?= base_url('proveedores/add_provider') ?>" class="needs-validation" novalidate="" method="post" accept-charset="utf-8">
                                <div class="form-row">
                                    <div class="col-12">
                                        <label for="validationCustom01">Nombre del proveedor</label>
                                        <input type="text" class="form-control" name="provider_name" required>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-12">
                                        <label for="validationCustom02">Correo</label>
                                        <input type="email" class="form-control" name="support_email" required>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-12">
                                        <label for="validationCustom03">Número de teléfono</label>
                                        <input type="text" class="form-control" name="support_telephone" required>
                                    </div>
                                </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Regresar <i class="fas fa-arrow-left"></i></button>
                            <button style="border-color: #001f3f;" class="bg-navy btn btn-primary" type="submit">Guardar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        <!-- /.Nuevos Proveedor -->
    <!-- /.Modal Porveedor -->

    <!-- Modal Cuentas -->
        <div class="modal fade" id="accounts" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Cuentas</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-4" style="height: 25rem;max-height: 25rem;">
                                <div class="overflow-auto os-viewport os-viewport-native-scrollbars-invisible">
                                    <div class="list-group" id="list-tab" role="tablist">
                                    <?php foreach($accounts as $key) { ?>    
                                        <div
                                            style="cursor:pointer" 
                                            class="text-truncate list-group-item list-group-item-action 
                                                <?= $first_account->id == $key->id ? 'active' : '' ?>"
                                            id="list-<?= $key->id ?>-list" 
                                            data-toggle="list" 
                                            href="#list-<?= $key->id ?>10000" 
                                            role="tab" 
                                            aria-controls="<?= $key->id ?>10000">
                                                <?= $key->account; ?>
                                        </div>
                                    <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-8">
                                <div class="tab-content" id="nav-tabContent">
                                    <?php $copy1=1; ?>
                                    <?php foreach($accounts as $key) { $copy1++;?>    
                                        <div class="tab-pane fade <?= $first_account->id == $key->id ? 'show active' : '' ?>" id="list-<?= $key->id ?>10000" role="tabpanel" aria-labelledby="list-<?= $key->id ?>10000-list">
                                            <h3 class="accountCopy text-truncate"><?= $key->account ?></h3>
                                            <div class="form-inline">
                                                <p style="cursor:pointer;"><i class="fas fa-globe-americas mr-2"></i><span class="siteCopy"><?= $key->site ?> </span> <a href="<?= $key->site ?>" target="_blank"><i class="fas fa-external-link-alt"></i></a></p>
                                            </div>
                                            <div style="cursor:pointer;" class="form-inline">
                                                <i class="fas fa-lock mr-2"></i>
                                                <p style="user-select: all!important;" id="text-copy-cuentas-1-<?= $copy1 ?>"><?= $key->password ?></p>
                                                <button class="copyButton badge badge-secondary mb-3 ml-2" data-clipboard-target="#text-copy-cuentas-1-<?= $copy1 ?>"><i class="far fa-clipboard"></i> Copy</button> 
                                            </div>
                                            
                                            <!-- Small Box (Stat card) -->
                                            <h5 class="mb-2 mt-4">Productos</h5>
                                            <div class="row">
                                                <!-- Conteo de dominios -->
                                                <div class="col-sm-6">
                                                    <div class="info-box bg-info">
                                                        <span class="info-box-icon"><i class="fas fa-globe"></i></span>

                                                        <div class="info-box-content">
                                                            <span class="info-box-text">Dominios</span>
                                                            <span class="info-box-number"><?= $key->domain_count ?></span>
                                                        </div>
                                                    <!-- /.info-box-content -->
                                                    </div>
                                                    <!-- /.info-box -->
                                                </div>
                                                <!-- Conteo de Hosting -->
                                                <div class="col-sm-6">
                                                    <div class="info-box bg-success">
                                                        <span class="info-box-icon"><i class="far fa-hdd"></i></span>

                                                        <div class="info-box-content">
                                                            <span class="info-box-text">Hosting</span>
                                                            <span class="info-box-number"><?= $key->hosting_count ?></span>
                                                        </div>
                                                        <!-- /.info-box-content -->
                                                    </div>
                                                    <!-- /.info-box -->
                                                </div>
                                                <!-- Conteo de Certificados SSL -->
                                                <div class="col-sm-6">
                                                    <div class="info-box bg-warning">
                                                    <span class="info-box-icon"><i class="fas fa-shield-alt"></i></span>

                                                    <div class="info-box-content">
                                                        <span class="info-box-text">Certificados SSL</span>
                                                        <span class="info-box-number">41,410</span>
                                                    </div>
                                                    <!-- /.info-box-content -->
                                                    </div>
                                                    <!-- /.info-box -->
                                                </div>
                                                <!-- Conteo de Servidores -->
                                                <div class="col-sm-6">
                                                    <div class="info-box bg-danger">
                                                    <span class="info-box-icon"><i class="fas fa-server"></i></span>

                                                    <div class="info-box-content">
                                                        <span class="info-box-text">Servidores</span>
                                                        <span class="info-box-number">41,410</span>
                                                    </div>
                                                    <!-- /.info-box-content -->
                                                    </div>
                                                    <!-- /.info-box -->
                                                </div>
                                            </div>
                                            <!-- /.row -->    
                                        </div>
                                        
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Regresar <i class="fas fa-arrow-left"></i></button>
                        <button type="button" style="border-color: #001f3f;" class="bg-navy btn btn-primary" data-toggle="modal" data-target="#newAccount">
                            Nueva cuenta
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Nuevas Cuentas -->
            <div class="modal fade" id="newAccount" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Nuevo Cuenta</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="<?= base_url('cuentas/add_account') ?>" class="needs-validation" novalidate="" method="post" accept-charset="utf-8">
                                <div class="form-row">
                                    <div class="col-12">
                                        <label for="validationCustom01">Nombre de la cuenta</label>
                                        <input type="text" class="form-control" name="account" required>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-12">
                                        <label for="validationCustom02">Sitio</label>
                                        <input type="text" class="form-control" name="site" required>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-12">
                                        <label for="validationCustom03">Contraseña</label>
                                        <input type="text" class="form-control" name="password" required>
                                    </div>
                                </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Regresar <i class="fas fa-arrow-left"></i></button>
                            <button  style="border-color: #001f3f;" class="bg-navy btn btn-primary" type="submit">Guardar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        <!-- /.Nuevas Cuentas -->
    <!-- /.Modal Cuentas -->

    <!-- Modal Clientes -->
        <div class="modal fade" id="customer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Clientes</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-4" style="height: 25rem;max-height: 25rem;">
                                <div class="overflow-auto os-viewport os-viewport-native-scrollbars-invisible">
                                    <div class="list-group" id="list-tab" role="tablist">
                                    <?php foreach($customer as $key) { ?>    
                                        <div
                                            style="cursor:pointer" 
                                            class="text-truncate list-group-item list-group-item-action 
                                                <?= $first_customer->id == $key->id ? 'active' : '' ?>"
                                            id="list-<?= $key->id ?>-list" 
                                            data-toggle="list" 
                                            href="#list-<?= $key->id ?>a" 
                                            role="tab" 
                                            aria-controls="<?= $key->id ?>a">
                                                <?= $key->customer_name; ?>
                                        </div>
                                    <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-8">
                                <div class="tab-content" id="nav-tabContent">
                                    <?php $copy2 = 1; ?>
                                    <?php foreach($customer as $key) { $copy2++?>    
                                        <div class="tab-pane fade <?= $first_customer->id == $key->id ? 'show active' : '' ?>" id="list-<?= $key->id ?>a" role="tabpanel" aria-labelledby="list-<?= $key->id ?>a-list">
                                            <h3 class="accountCopy text-truncate"><?= $key->customer_name ?></h3>
                                            <div style="cursor:pointer;" class="form-inline">
                                                <i class="fas fa-at mr-2"></i>
                                                <p style="user-select: all!important;" id="text-copy-clientes-1-<?= $copy2 ?>" ><?= $key->customer_email ?></p>
                                                <button data-clipboard-target="#text-copy-clientes-1-<?= $copy2 ?>" class="copyButton badge badge-secondary mb-3 ml-2"><i class="far fa-clipboard"></i> Copy</button> 
                                            </div>
                                            <div style="cursor:pointer;" class="form-inline">
                                                <i class="fas fa-phone-alt mr-2"></i>
                                                <p id="text-copy-clientes-2-<?= $copy2 ?>" ><?= $key->customer_telephone ?></p>
                                                <button data-clipboard-target="#text-copy-clientes-2-<?= $copy2 ?>" class="copyButton badge badge-secondary mb-3 ml-2"><i class="far fa-clipboard"></i> Copy</button> 
                                            </div>

                                            <!-- Small Box (Stat card) -->
                                            <h5 class="mb-2 mt-4">Productos</h5>
                                            <div class="row">
                                                <!-- Conteo de dominios -->
                                                <div class="col-sm-6">
                                                    <div class="info-box bg-info">
                                                        <span class="info-box-icon"><i class="fas fa-globe"></i></span>

                                                        <div class="info-box-content">
                                                            <span class="info-box-text">Dominios</span>
                                                            <span class="info-box-number"><?= $key->domain_count ?></span>
                                                        </div>
                                                    <!-- /.info-box-content -->
                                                    </div>
                                                    <!-- /.info-box -->
                                                </div>
                                                <!-- Conteo de Hosting -->
                                                <div class="col-sm-6">
                                                    <div class="info-box bg-success">
                                                        <span class="info-box-icon"><i class="far fa-hdd"></i></span>

                                                        <div class="info-box-content">
                                                            <span class="info-box-text">Hosting</span>
                                                            <span class="info-box-number"><?= $key->hosting_count ?></span>
                                                        </div>
                                                        <!-- /.info-box-content -->
                                                    </div>
                                                    <!-- /.info-box -->
                                                </div>
                                                <!-- Conteo de Certificados SSL -->
                                                <div class="col-sm-6">
                                                    <div class="info-box bg-warning">
                                                    <span class="info-box-icon"><i class="fas fa-shield-alt"></i></span>

                                                    <div class="info-box-content">
                                                        <span class="info-box-text">Certificados SSL</span>
                                                        <span class="info-box-number">41,410</span>
                                                    </div>
                                                    <!-- /.info-box-content -->
                                                    </div>
                                                    <!-- /.info-box -->
                                                </div>
                                                <!-- Conteo de Servidores -->
                                                <div class="col-sm-6">
                                                    <div class="info-box bg-danger">
                                                    <span class="info-box-icon"><i class="fas fa-server"></i></span>

                                                    <div class="info-box-content">
                                                        <span class="info-box-text">Servidores</span>
                                                        <span class="info-box-number">41,410</span>
                                                    </div>
                                                    <!-- /.info-box-content -->
                                                    </div>
                                                    <!-- /.info-box -->
                                                </div>
                                            </div>
                                            <!-- /.row -->    
                                        </div>
                                        
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Regresar <i class="fas fa-arrow-left"></i></button>
                        <button type="button" style="border-color: #001f3f;" class="bg-navy btn btn-primary" data-toggle="modal" data-target="#newCustomer">
                            Nuevo cliente
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Nuevos Clientes -->
            <div class="modal fade" id="newCustomer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Nuevo Cliente</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="<?= base_url('clientes/add_customer') ?>" class="needs-validation" novalidate="" method="post" accept-charset="utf-8">
                                <div class="form-row">
                                    <div class="col-12">
                                        <label for="validationCustom01">Nombre del cliente</label>
                                        <input type="text" class="form-control" name="customer_name" required>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-12">
                                        <label for="validationCustom02">Correo</label>
                                        <input type="email" class="form-control" name="customer_email" required>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-12">
                                        <label for="validationCustom03">Número de teléfono</label>
                                        <input type="text" class="form-control" name="customer_telephone" required>
                                    </div>
                                </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Regresar <i class="fas fa-arrow-left"></i></button>
                            <button style="border-color: #001f3f;" class="bg-navy btn btn-primary" type="submit">Guardar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        <!-- /.Nuevos Clientes -->
    <!-- /.Modal Clientes -->


<!-- /.MODALS -->



<script>
    function copyToClipboard(element) {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(element).text()).select();
        document.execCommand("copy");
        $temp.remove();
    }
</script>
    <script>
        // Example starter JavaScript for disabling form submissions if there are invalid fields
        (function() {
        'use strict';
        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
            });
        }, false);
        })();
    </script>