<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Hosting_model extends CI_Model {


  // ------------------------------------------------------------------------
  
  public function index()
  {
    $query = $this->db->query(
      "SELECT 
      h.id,
      h.hosting_name,
      pr.provider_name,
      a.account,
      p.name_pack,
      s.server_name,
      c.customer_name,
      h.domain_name,
      h.creation_date,
      h.hosting_expiry
      FROM hosting h
      INNER JOIN servers s ON h.id_server_name = s.id
      INNER JOIN customers c ON h.id_customer_name = c.id
      INNER JOIN hosting_package p ON h.id_package = p.id
      INNER JOIN providers pr ON h.id_package = pr.id
      INNER JOIN accounts a ON h.id_account = a.id
      WHERE h.status = 1
      ORDER BY h.hosting_name
    "
    );
    return $query->result();
  }

  // ------------------------------------------------------------------------ 

  public function record_count() {
    $this->db->where("hosting_domain","1");
    return $this->db->count_all("hosting");
  }

  // ------------------------------------------------------------------------
  
  public function get_Hosting_Package()
  {
    $query = $this->db->query("SELECT * FROM hosting_package");
    return $query->result();
  }

  public function get_Servers()
  {
    $query = $this->db->query("SELECT id, server_name FROM servers");
    return $query->result();
  }

  public function get_Customer()
  {
    $query = $this->db->query("SELECT * FROM customers");
    return $query->result();
  }

  public function get_Domains()
  {
    $query = $this->db->query("SELECT id, domain_name FROM domains");
    return $query->result();
  }

  public function add_customer($data) {
    return $this->db->insert('customers', $data);
  }

  public function get_Customer_by_last_id()
  {
    $query = $this->db->query("SELECT id FROM customers ORDER BY id DESC LIMIT 1");
    return $query->result();
  }

  public function insert($data) {
    return $this->db->insert('hosting', $data);
  }

  public function get_by_id($id){
    $query = $this->db->query("SELECT * FROM hosting WHERE id = $id");
    return $query->row();
  }

  public function update($id, $a){
    $this->db->where('id', $id);
    $this->db->update('hosting', $a);
  }

  public function update_remove($id=-1){
    if($id != -1){
      $s = "UPDATE hosting SET status = 0 WHERE id = $id";
      $this->db->query($s);
    }
    return -1;
  }
  public function get_Providers(){
    $query = $this->db->query(
      "SELECT p.*,
      (SELECT COUNT(*) FROM domains d WHERE d.id_provider = p.id) AS domains_count,
      (SELECT COUNT(*) FROM hosting h WHERE h.id_provider = p.id) AS hosting_count
      FROM providers p
      ");
      return $query->result();
  }
  public function get_Accounts(){
    $query = $this->db->query(
      "SELECT a.*,
      (SELECT COUNT(*) FROM domains d WHERE (d.id_account = a.id AND d.status = 1)) AS domain_count,
      (SELECT COUNT(*) FROM hosting h WHERE h.id_account = a.id) AS hosting_count
      FROM accounts a
      ");
    return $query->result();
  }
  public function get_First_Providers()
  {
    $query = $this->db->query("SELECT id FROM `providers` LIMIT 1");
    return $query->row();
  }
  public function get_First_Account()
  {
    $query = $this->db->query("SELECT id FROM `accounts` LIMIT 1");
    return $query->row();
  }
  public function get_Customer_Newdomain(){
    $query = $this->db->query(
      "SELECT c.*,
      (SELECT COUNT(*) FROM domains d WHERE (d.id_customer = c.id AND d.status = 1)) AS domain_count,
      (SELECT COUNT(*) FROM hosting h WHERE h.id_customer_name = c.id) AS hosting_count
      FROM customers c
      ");
    return $query->result();
  }
  public function get_First_Customer()
  {
    $query = $this->db->query("SELECT id FROM `customers` LIMIT 1");
    return $query->row();
  }

}