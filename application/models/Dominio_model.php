<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Dominio_model extends CI_Model {


  // ------------------------------------------------------------------------
  
  public function index()
  {
    $query = $this->db->query(
      "SELECT 
      d.id, 
      d.domain_name, 
      p.provider_name, 
      d.cost, 
      d.sale, 
      d.creation_date, 
      d.expiration_date, 
      c.customer_name, 
      a.account
      FROM domains d
      INNER JOIN providers p ON d.id_provider = p.id
      INNER JOIN customers c ON d.id_customer = c.id
      INNER JOIN accounts a ON d.id_account = a.id
      WHERE d.status = 1
      ORDER BY d.domain_name"
    );
    return $query->result_array();
  }

  // ------------------------------------------------------------------------ 

  public function get_Providers(){
    $query = $this->db->query(
      "SELECT p.*,
      (SELECT COUNT(*) FROM domains d WHERE d.id_provider = p.id) AS domains_count,
      (SELECT COUNT(*) FROM hosting h WHERE h.id_provider = p.id) AS hosting_count
      FROM providers p
      ");
      return $query->result();
  }

  public function record_count() {
    $this->db->where("name_domain","1");
    return $this->db->count_all("domains");
  }

  public function get_First_Providers()
  {
    $query = $this->db->query("SELECT id FROM `providers` LIMIT 1");
    return $query->row();
  }

  public function get_Type()
  {
    $query = $this->db->query("SELECT * FROM type_domain");
    return $query->result();
  }

  public function get_Customer()
  {
    $query = $this->db->query("SELECT * FROM customers");
    return $query->result();
  }

  public function add_domain($data) {
    return $this->db->insert('domains', $data);
  }

  public function insert($data) {
    return $this->db->insert('domains', $data);
  }

  public function get_by_id($id){
    $query = $this->db->query("SELECT * FROM domains WHERE id = $id");
    return $query->row();
  }

  public function update($id, $a){
    $this->db->where('id', $id);
    $this->db->update('domains', $a);
  }

  public function get_Accounts(){
    $query = $this->db->query(
      "SELECT a.*,
      (SELECT COUNT(*) FROM domains d WHERE (d.id_account = a.id AND d.status = 1)) AS domain_count,
      (SELECT COUNT(*) FROM hosting h WHERE h.id_account = a.id) AS hosting_count
      FROM accounts a
      ");
    return $query->result();
  }

  public function get_First_Account()
  {
    $query = $this->db->query("SELECT id FROM `accounts` LIMIT 1");
    return $query->row();
  }

  public function get_Customer_Newdomain(){
    $query = $this->db->query(
      "SELECT c.*,
      (SELECT COUNT(*) FROM domains d WHERE (d.id_customer = c.id AND d.status = 1)) AS domain_count,
      (SELECT COUNT(*) FROM hosting h WHERE h.id_customer_name = c.id) AS hosting_count
      FROM customers c
      ");
    return $query->result();
  }

  public function get_First_Customer()
  {
    $query = $this->db->query("SELECT id FROM `customers` LIMIT 1");
    return $query->row();
  }

  public function get_Account()
  {
    $query = $this->db->query("SELECT * FROM accounts");
    return $query->result();
  }

  public function update_remove($id=-1){
    if($id != -1){
      $s = "UPDATE domains SET status = 0 WHERE id = $id";
      $this->db->query($s);
    }
    return -1;
  }
}