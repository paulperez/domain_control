<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model("Clientes_model");
		$this->load->helper('form');
		$this->load->helper('url');
    }

	public function add_customer()
	{
        $data['customer_name'] = $this->input->post('customer_name');
        $data['customer_email'] = $this->input->post('customer_email');
        $data['customer_telephone'] = $this->input->post('customer_telephone');
        
        
        $this->Clientes_model->insert($data);
        redirect('dominios/newdomain');        
	}
}
