<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dominios extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model("Dominio_model");
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('session');

    }

	public function index(){
		$a['domain_menu'] = true;
        $this->load->view('Template/header', $a);
		$q["dominios"] = $this->Dominio_model->index();
        $this->load->view('dominios/index', $q);
        $this->load->view('Template/footer');
	}

	public function create($id = null){
		$this->load->helper('form');
		$this->load->helper('url');

		$this->form_validation->set_rules('domain_name', 'Nombre de Dominio', 'required');
		$this->form_validation->set_rules('id_provider', 'Proveedor', 'required');
		$this->form_validation->set_rules('cost', 'Costo', 'required');
		$this->form_validation->set_rules('creation_date', 'Fecha de Creacióm', 'required');
		$this->form_validation->set_rules('expiration_date', 'Fecha de Expidarción', 'required');
		$this->form_validation->set_rules('id_customer', 'Cliente', 'required');
		$this->form_validation->set_rules('id_account', 'Cuenta', 'required');

		$a['id'] = $a['domain_name'] = $a['id_provider'] = $a['cost'] = $a['sale'] = $a['creation_date'] = $a['expiration_date'] = $a['id_customer'] = $a['id_account'] = "";
		
		if (isset($id)) {
			$dominio_id = $this->Dominio_model->get_by_id($id);

			if (isset($dominio_id)) {
				$a['id'] = $dominio_id->id;
				$a['domain_name'] = $dominio_id->domain_name;
				$a['id_provider'] = $dominio_id->id_provider;
				$a['cost'] = $dominio_id->cost;
				$a['sale'] = $dominio_id->sale;
				$a['creation_date'] = $dominio_id->creation_date;
				$a['expiration_date'] = $dominio_id->expiration_date;
				$a['id_customer'] = $dominio_id->id_customer;
				$a['id_account'] = $dominio_id->id_account;
			}
		}
		


		$as['add_domain'] = true;
		$as['providers'] = $this->Dominio_model->get_Providers();
		$as['customer'] = $this->Dominio_model->get_Customer();
		$as['account'] = $this->Dominio_model->get_Account();

		if ($this->input->server("REQUEST_METHOD") == "POST") {
			$data['domain_name'] = $this->input->post('domain_name');
			$data['id_provider'] = $this->input->post('id_provider');
			$data['cost'] = $this->input->post('cost');
			$data['sale'] = $this->input->post('sale');
			$data['creation_date'] = $this->input->post('creation_date');
			$data['expiration_date'] = $this->input->post('expiration_date');
			$data['id_customer'] = $this->input->post('id_customer');
			$data['id_account'] = $this->input->post('id_account');
			
			$a['domain_name'] = $this->input->post('domain_name');
			$a['id_provider'] = $this->input->post('id_provider');
			$a['cost'] = $this->input->post('cost');
			$a['sale'] = $this->input->post('sale');
			$a['creation_date'] = $this->input->post('creation_date');
			$a['expiration_date'] = $this->input->post('expiration_date');
			$a['id_customer'] = $this->input->post('id_customer');
			$a['id_account'] = $this->input->post('id_account');
			
			if ($this->form_validation->run() !== false){

				if (isset($id)) {
					$this->Dominio_model->update($id, $a);
					$this->session->set_flashdata('message', 'El dominio '.$a['domain_name'].' fué actualizado con éxito!');

					redirect('dominios/index');
				} else {
					$this->Dominio_model->insert($data);
					$this->session->set_flashdata('message', 'El dominio '.$data['domain_name'].' fué ingresado con éxito!');

					redirect('dominios/index');
				}
			}
		}


        $this->load->view('Template/header', $as);
        $this->load->view('dominios/create', $a);
        $this->load->view('Template/footer');
		
	}

	public function newdomain(){
		$as['add_domain'] = true;
		$a['providers'] = $this->Dominio_model->get_Providers();
		$a['accounts'] = $this->Dominio_model->get_Accounts();
		$a['first_provider'] = $this->Dominio_model->get_First_Providers();
		$a['first_account'] = $this->Dominio_model->get_First_Account();
		$a['customer'] = $this->Dominio_model->get_Customer_Newdomain();
		$a['first_customer'] = $this->Dominio_model->get_First_Customer();

        $this->load->view('Template/header', $as);
        $this->load->view('dominios/newdomain', $a);
        $this->load->view('Template/footer');
	}

	public function remove($id = -1){
        $this->load->helper('form');
        $this->load->library('form_validation');
        if ($this->Dominio_model->update_remove($id)) {
            redirect(base_url('dominios/index'));
		}
	}

}