<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model("Dominio_model");
		$this->load->model("Hosting_model");
    }

	public function index()
	{
        $this->load->view('Template/header');
		$q['allDomains'] = $this->Dominio_model->record_count();
		$q['allHosting'] = $this->Hosting_model->record_count();
        $this->load->view('home/index', $q);
        $this->load->view('Template/footer');
	}
}
