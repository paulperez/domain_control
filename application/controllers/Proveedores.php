<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proveedores extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model("Proveedor_model");
		$this->load->model("Dominio_model");
		$this->load->helper('form');
		$this->load->helper('url');
	}
	
	public function index(){
		$as['add_domain'] 		= true;
		$a['providers'] 		= $this->Dominio_model->get_Providers();
		$a['first_provider'] 	= $this->Dominio_model->get_First_Providers();
		$a['accounts'] 			= $this->Dominio_model->get_Accounts();
		$a['first_account'] 	= $this->Dominio_model->get_First_Account();
		$a['customer'] 			= $this->Dominio_model->get_Customer_Newdomain();
		$a['first_customer'] 	= $this->Dominio_model->get_First_Customer();

        $this->load->view('Template/header', $as);
        $this->load->view('proveedores/index', $a);
        $this->load->view('Template/footer');
	}

	public function add_provider()
	{
        $data['provider_name'] 		= $this->input->post('provider_name');
        $data['support_email'] 		= $this->input->post('support_email');
        $data['support_telephone'] 	= $this->input->post('support_telephone');
        
        
        $this->Proveedor_model->insert($data);
        redirect('dominios/newdomain');        
	}
}
