<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cuentas extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model("Cuentas_model");
		$this->load->helper('form');
		$this->load->helper('url');
    }

	public function add_account()
	{
        $data['site'] = $this->input->post('site');
        $data['account'] = $this->input->post('account');
        $data['password'] = $this->input->post('password');
        
        
        $this->Cuentas_model->insert($data);
        redirect('dominios/newdomain');        
	}
}
