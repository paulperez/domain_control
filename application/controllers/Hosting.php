<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hosting extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model("Hosting_model");
		$this->load->helper('url');
		$this->load->library('form_validation');
		$this->load->library('session');

    }

	public function index()
	{
		$a['hosting_menu'] = true;
		$a["hosting_list"] = $this->Hosting_model->index();
        $this->load->view('Template/header', $a);
        $this->load->view('hosting/index');
        $this->load->view('Template/footer');
	}

	public function create($id = null)
	{
		$this->load->helper('form');
		$this->load->helper('url');

		$a['id'] = "";
		$a['hosting_name_by_id'] = "";
		$a['pack_by_id'] = "";
		$a['servers_by_id'] = "";
		$a['customer_by_id'] = "";
		$a['domain_name_by_id'] = "";
		$a['expiration_date_by_id'] = "";
		
		if (isset($id)) {
			$hosting_id = $this->Hosting_model->get_by_id($id);

			if (isset($hosting_id)) {
				$a['id'] = $hosting_id->id;
				$a['hosting_name_by_id'] = $hosting_id->hosting_name;
				$a['pack_by_id'] = $hosting_id->id_package;
				$a['servers_by_id'] = $hosting_id->id_server_name;
				$a['customer_by_id'] = $hosting_id->id_customer_name;
				$a['domain_name_by_id'] = $hosting_id->domain_name;
				$a['expiration_date_by_id'] = $hosting_id->hosting_expiry;
			}
		}

		$as['add_hosting'] = true;
		$as['paquetes'] = $this->Hosting_model->get_Hosting_Package();
		$as['servidores'] = $this->Hosting_model->get_Servers();
		$as['cliente'] = $this->Hosting_model->get_Customer();
		$as['domains'] = $this->Hosting_model->get_Domains();


		if ($this->input->server("REQUEST_METHOD") == "POST") {
			$data['hosting_name'] = $this->input->post('hosting_name');
			$data['id_package'] = $this->input->post('pack');
			$data['id_server_name'] = $this->input->post('servers');
			$data['id_customer_name'] = $this->input->post('customer');
			$data['domain_name'] = $this->input->post('domain_name');
			$data['hosting_expiry'] = $this->input->post('hosting_expiry');
			
			$a['hosting_name'] = $this->input->post('hosting_name');
			$a['id_package'] = $this->input->post('pack');
			$a['id_server_name'] = $this->input->post('servers');
			$a['id_customer_name'] = $this->input->post('customer');
			$a['domain_name'] = $this->input->post('domain_name');
			$a['expiration_date'] = $this->input->post('expiration_date');
			
			if (isset($id)) {
				$this->Hosting_model->update($id, $a);
				$this->session->set_flashdata('message', 'El dominio '.$a['hosting_name'].' fué actualizado con éxito!');

				redirect('hosting');
			} else {
				$this->Hosting_model->insert($data);
				$this->session->set_flashdata('message', 'El dominio '.$data['hosting_name'].' fué ingresado con éxito!');

				redirect('hosting');
			}
		}
		

		
		
        $this->load->view('Template/header', $as);
        $this->load->view('hosting/create', $a);
        $this->load->view('Template/footer');
		
	}

	public function add_customer()
	{
		if ($this->input->is_ajax_request()) {
			$this->form_validation->set_rules('customer_name', 'Name', 'required');
			$this->form_validation->set_rules('customer_email', 'Email', 'required|valid_email');

			if ($this->form_validation->run() == false) {
				$data = array('responce' => 'error', 'message' => validation_errors());
			} else {

				$ajax_data = $this->input->post();
				if ($this->Hosting_model->add_customer($ajax_data)) {
					$data = array('responce' => 'success', 'message' => 'Record added Successfully');
				}else{
					$data = array('responce' => 'error', 'message' => 'failed');
				}
			}
			echo json_encode($data);
			
		} else {
			echo "No direct script access allowed";
		}
		
		$this->load->helper('form');
		$this->load->helper('url');
		
	}

	public function remove($id = -1){
        $this->load->helper('form');
        $this->load->library('form_validation');
        if ($this->Hosting_model->update_remove($id)) {
            redirect(base_url('hosting/index'));
		}
	}
	

	public function newhosting(){
		$as['add_hosting'] = true;
		$a['providers'] = $this->Hosting_model->get_Providers();
		$a['accounts'] = $this->Hosting_model->get_Accounts();
		$a['first_provider'] = $this->Hosting_model->get_First_Providers();
		$a['first_account'] = $this->Hosting_model->get_First_Account();
		$a['customer'] = $this->Hosting_model->get_Customer_Newdomain();
		$a['first_customer'] = $this->Hosting_model->get_First_Customer();

        $this->load->view('Template/header', $as);
        $this->load->view('hosting/newhosting', $a);
        $this->load->view('Template/footer');
	}
}